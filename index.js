const CITY_ID = 311; //Porto
const CITY_COORDS = {
	lat: 41.1579438,
	lng: -8.6291053
};
const OPTIONS = {
	headers: {
		'user-key':'c24ba7a7fe424b96ed72f7f49c10bf9a'
	}
};
let map;

function initMap () {
	
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: CITY_COORDS
	});
	setMarkers();
}

let setMarkers = () => {
	
	fetch(`https://developers.zomato.com/api/v2.1/search?entity_id=${CITY_ID}&entity_type=city`, OPTIONS)
		.then(response => response.json())
		.then(data => {
			let restaurants = normalizeRestaurantsData(data.restaurants);
			restaurants.forEach(restaurant => {
				//const coordinates = normalizeCoord(restaurant.location);
				const { name, coordinates } = restaurant;
				const marker = new google.maps.Marker({
					title: name,
					position: coordinates,
					map: map
				});
			});
		})
		.catch(error => console.log(error));
};

let normalizeRestaurantsData = (data) => {
	return data.map(item => {
		let { restaurant } = item;
		let { latitude, longitude } = restaurant.location;
		restaurant.coordinates = {
			lat: Number(latitude),
			lng: Number(longitude)
		};
		
		return restaurant;
	})
};

/*
let normalizeCoord = (location) => {
	
	return {
		lat: Number(location.latitude),
		lng: Number(location.longitude)
	}
};*/
